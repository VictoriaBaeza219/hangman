package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.sun.deploy.util.StringUtils;
import components.AppDataComponent;
import data.GameData;
import data.GameDataFile;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author Victoria
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData gamedata;    // shared reference to the game being played, loaded or saved
    private Text[] progress;    // reference to the text area for the word
    private boolean success;     // whether or not player was successful
    private int discovered;  // the number of letters already discovered
    private Button gameButton;  // shared reference to the "start game" button
    private Label remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean gameover;    // whether or not the current game is already over
    private boolean savable;
    private Path workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        gamedata = new GameData(appTemplate);
        appTemplate.setDataComponent(gamedata);
        gameButton.setDisable(true);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        workFile = null;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        play();
    }

    //replace with GUI
    private void end() {
        //appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        if (success == false) {
            AppMessageDialogSingleton messenger = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            messenger.show(props.getPropertyValue(LOSER_TITLE), props.getPropertyValue(LOSER_MESSAGE) + gamedata.getTargetWord());
        } else {
            AppMessageDialogSingleton messenger = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            messenger.show(props.getPropertyValue(WINNER_TITLE), props.getPropertyValue(WINNER_MESSAGE));
        }


    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if (!alreadyGuessed(guess) && (Character.isAlphabetic(guess) || guess == '\'')) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess)
                            gamedata.addBadGuess(guess);
                        savable = true;
                        appTemplate.getGUI().updateWorkspaceToolbar(savable);
                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    stop();

                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        boolean makenew = true;
        if (savable)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            savable = false;                                       // prevents saving before the game begins
            appTemplate.getGUI().updateWorkspaceToolbar(savable);   //if made new, prevents saving empty game
            enableGameButton();
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        boolean dummy = false;
        try {
            if (savable) {
                dummy = promptToSave();
            } else {
                //should never be needed if the "savable" variable is used properly, but this is here just in case.
                //If the
                messageDialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            }

        } catch (Exception ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    @Override
    public void handleLoadRequest() {
        boolean startOver = false;
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();
        try {
            if (savable) {
                startOver = promptToSave(); //Done so the user is prompted to save, but also determines if we will load
                if (startOver) { //Wipe clean
                    //appTemplate.getDataComponent().reset();
                    //appTemplate.getWorkspaceComponent().reloadWorkspace();
                    //ensureActivatedWorkspace();
                    //workFile = null;
                    //Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                    //gameWorkspace.reinitialize();
                    savable = false;
                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
                }
            }
        } catch (IOException ioe) {
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
        if (!savable || startOver) {
            try {
                FileChooser opener = new FileChooser();
                opener.setTitle("Load Hangman Game File");
                opener.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter(props.getPropertyValue(WORK_FILE_EXT_DESC),
                                props.getPropertyValue(WORK_FILE_EXT)));
                File selectedFile = opener.showOpenDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    int size = (Paths.get(selectedFile.getAbsolutePath()).toString()).length();
                    //if the wrong path has been specified through out selectedFile, we do not want to load any more.
                    if (!((Paths.get(selectedFile.getAbsolutePath()).toString()).regionMatches(size-5, ".json", 0, 5))){
                        dialog.show(props.getPropertyValue(LOAD_ERROR_TITLE),
                                props.getPropertyValue(LOAD_ERROR_MESSAGE_EXT));
                    }
                    else {
                        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), Paths.get(selectedFile.getAbsolutePath()));
                    }
                }
            } catch (Exception ioe2) {
                dialog.show(props.getPropertyValue(LOAD_ERROR_TITLE), props.getPropertyValue(LOAD_ERROR_MESSAGE));
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable)
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        YesNoCancelDialogSingleton yesNoDialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        yesNoDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
        String selection = yesNoDialog.getSelection(); //get the selection from the user
        //Returning yes (then make new)
        if (selection.equals(YesNoCancelDialogSingleton.YES)) {
            boolean isExtError = false;

            if (workFile == null) { //checks for an already existing file location for the game data
                FileChooser chooser = new FileChooser();
                //chooser.setInitialDirectory(new File("C:/Users/Victoria/Desktop/CSE219/Homework1/TheHangmanGame/Hangman/resources/saved"));
                chooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                chooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
                                propertyManager.getPropertyValue(WORK_FILE_EXT)));

                File selectedFile = chooser.showSaveDialog(appTemplate.getGUI().getWindow());
                int size = (Paths.get(selectedFile.getAbsolutePath()).toString()).length();
                //if the wrong path has been specified through out selectedFile, we do not want to save any more.
                if (!((Paths.get(selectedFile.getAbsolutePath()).toString()).regionMatches(size - 5, ".json", 0, 5))) {
                    AppMessageDialogSingleton appDialouge = AppMessageDialogSingleton.getSingleton();
                    appDialouge.show(propertyManager.getPropertyValue(LOAD_ERROR_TITLE),
                            propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE_EXT));
                    isExtError = true;
                }
                else {
                    workFile = Paths.get(selectedFile.getAbsolutePath());
                }
                if (!isExtError && selectedFile != null) {
                    appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), Paths.get(selectedFile.getAbsolutePath()));
                    savable = false;
                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
                    AppMessageDialogSingleton messenger = AppMessageDialogSingleton.getSingleton();
                    messenger.show(propertyManager.getPropertyValue(SAVE_COMPLETED_TITLE),
                            propertyManager.getPropertyValue(SAVE_COMPLETED_MESSAGE));
                } else if (!isExtError){
                    appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), workFile.toAbsolutePath());
                    savable = false;
                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
                    AppMessageDialogSingleton messenger = AppMessageDialogSingleton.getSingleton();
                    messenger.show(propertyManager.getPropertyValue(SAVE_COMPLETED_TITLE),
                            propertyManager.getPropertyValue(SAVE_COMPLETED_MESSAGE));
                }
            }

            else if (workFile != null) { //allows for overwriting if a file already exists in this game data
                appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), workFile.toAbsolutePath());
                savable = false;
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                AppMessageDialogSingleton messenger = AppMessageDialogSingleton.getSingleton();
                messenger.show(propertyManager.getPropertyValue(SAVE_COMPLETED_TITLE),
                        propertyManager.getPropertyValue(SAVE_COMPLETED_MESSAGE));
            }
            return true;
        }
        //Returning no.
        else if (selection.equals(YesNoCancelDialogSingleton.NO)) {
            savable = true;
            return true;
        }
        //Returning cancel.
        else {
            savable = true;
            return false;
        }
    }


    public void setSavable(boolean b) {
        savable = b;
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    //unused, replaced with saveData method
    private void save(Path target) throws IOException {
        //note that every game will always have a FALSE success
        File f = new File(target.toString());
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(f, gamedata);

    }
}
