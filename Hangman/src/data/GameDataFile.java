package data;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

/**
 * @author Ritwik Banerjee
 * @author Victoria
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException{
        GameData gamedata = (GameData)data;
        File f = new File(to.toString());
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(f, gamedata);
    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData toBeLoaded = (GameData)data;
        ObjectMapper mapper = new ObjectMapper();
        toBeLoaded = (mapper.readValue(new File(from.toString()), toBeLoaded.getClass()));
        System.out.println((new File(from.toString())).toString());
        System.out.println(from.toString());
        System.out.println((toBeLoaded.getTargetWord()));

    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
